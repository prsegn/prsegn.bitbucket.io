
var styleJSON = {
    "version": 8,
    "name": "qgis2web export",
    "pitch": 0,
    "light": {
        "intensity": 0.2
    },
    "sources": {
        "gis_osm_waterways_free_1_0": {
            "type": "geojson",
            "data": json_gis_osm_waterways_free_1_0
        }
                    ,
        "gis_osm_water_a_free_1_1": {
            "type": "geojson",
            "data": json_gis_osm_water_a_free_1_1
        }
                    ,
        "gis_osm_transport_free_1_2": {
            "type": "geojson",
            "data": json_gis_osm_transport_free_1_2
        }
                    ,
        "gis_osm_transport_a_free_1_3": {
            "type": "geojson",
            "data": json_gis_osm_transport_a_free_1_3
        }
                    ,
        "gis_osm_traffic_free_1_4": {
            "type": "geojson",
            "data": json_gis_osm_traffic_free_1_4
        }
                    ,
        "gis_osm_traffic_a_free_1_5": {
            "type": "geojson",
            "data": json_gis_osm_traffic_a_free_1_5
        }
                    ,
        "gis_osm_roads_free_1_6": {
            "type": "geojson",
            "data": json_gis_osm_roads_free_1_6
        }
                    ,
        "gis_osm_railways_free_1_7": {
            "type": "geojson",
            "data": json_gis_osm_railways_free_1_7
        }
                    ,
        "gis_osm_pois_free_1_8": {
            "type": "geojson",
            "data": json_gis_osm_pois_free_1_8
        }
                    ,
        "gis_osm_pois_a_free_1_9": {
            "type": "geojson",
            "data": json_gis_osm_pois_a_free_1_9
        }
                    ,
        "gis_osm_pofw_free_1_10": {
            "type": "geojson",
            "data": json_gis_osm_pofw_free_1_10
        }
                    ,
        "gis_osm_pofw_a_free_1_11": {
            "type": "geojson",
            "data": json_gis_osm_pofw_a_free_1_11
        }
                    ,
        "gis_osm_places_free_1_12": {
            "type": "geojson",
            "data": json_gis_osm_places_free_1_12
        }
                    ,
        "gis_osm_places_a_free_1_13": {
            "type": "geojson",
            "data": json_gis_osm_places_a_free_1_13
        }
                    ,
        "gis_osm_natural_free_1_14": {
            "type": "geojson",
            "data": json_gis_osm_natural_free_1_14
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_sougueta": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_sougueta
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_nianaya": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_nianaya
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_dbl": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_dbl
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_mambia": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_mambia
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_linsan": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_linsan
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_koliagb": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_koliagb
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_friguiagb": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_friguiagb
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_centreville": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_centreville
        }
                    ,
        "POSTE_NON_TRAITES_LOCALITES_KINDIA_kolenten": {
            "type": "geojson",
            "data": json_POSTE_NON_TRAITES_LOCALITES_KINDIA_kolenten
        }
                    },
    "sprite": "",
    "glyphs": "https://glfonts.lukasmartinelli.ch/fonts/{fontstack}/{range}.pbf",
    "layers": [
        {
            "id": "background",
            "type": "background",
            "layout": {},
            "paint": {
                "background-color": "#ffffff"
            }
        },
        {
            "id": "lyr_gis_osm_waterways_free_1_0_0",
            "type": "line",
            "source": "gis_osm_waterways_free_1_0",
            "layout": {},
            "paint": {'line-width': 0.9285714285714285, 'line-opacity': 1.0, 'line-color': '#c43c39'}
        }
,
        {
            "id": "lyr_gis_osm_water_a_free_1_1_0",
            "type": "fill",
            "source": "gis_osm_water_a_free_1_1",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#e15989'}
        }
,
        {
            "id": "lyr_gis_osm_transport_free_1_2_0",
            "type": "circle",
            "source": "gis_osm_transport_free_1_2",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#987db7', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_gis_osm_transport_a_free_1_3_0",
            "type": "fill",
            "source": "gis_osm_transport_a_free_1_3",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#b7484b'}
        }
,
        {
            "id": "lyr_gis_osm_traffic_free_1_4_0",
            "type": "circle",
            "source": "gis_osm_traffic_free_1_4",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#e8718d', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_gis_osm_traffic_a_free_1_5_0",
            "type": "fill",
            "source": "gis_osm_traffic_a_free_1_5",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#f3a6b2'}
        }
,
        {
            "id": "lyr_gis_osm_roads_free_1_6_0",
            "type": "line",
            "source": "gis_osm_roads_free_1_6",
            "layout": {},
            "paint": {'line-width': 0.9285714285714285, 'line-opacity': 1.0, 'line-color': '#d5b43c'}
        }
,
        {
            "id": "lyr_gis_osm_railways_free_1_7_0",
            "type": "line",
            "source": "gis_osm_railways_free_1_7",
            "layout": {},
            "paint": {'line-width': 0.9285714285714285, 'line-opacity': 1.0, 'line-color': '#85b66f'}
        }
,
        {
            "id": "lyr_gis_osm_pois_free_1_8_0",
            "type": "circle",
            "source": "gis_osm_pois_free_1_8",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#91522d', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_gis_osm_pois_a_free_1_9_0",
            "type": "fill",
            "source": "gis_osm_pois_a_free_1_9",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#c43c39'}
        }
,
        {
            "id": "lyr_gis_osm_pofw_free_1_10_0",
            "type": "circle",
            "source": "gis_osm_pofw_free_1_10",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#e15989', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_gis_osm_pofw_a_free_1_11_0",
            "type": "fill",
            "source": "gis_osm_pofw_a_free_1_11",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#987db7'}
        }
,
        {
            "id": "lyr_gis_osm_places_free_1_12_0",
            "type": "circle",
            "source": "gis_osm_places_free_1_12",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#b7484b', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_gis_osm_places_a_free_1_13_0",
            "type": "fill",
            "source": "gis_osm_places_a_free_1_13",
            "layout": {},
            "paint": {'fill-opacity': 1.0, 'fill-color': '#e8718d'}
        }
,
        {
            "id": "lyr_gis_osm_natural_free_1_14_0",
            "type": "circle",
            "source": "gis_osm_natural_free_1_14",
            "layout": {},
            "paint": {'circle-radius': ['/', 7.142857142857142, 2], 'circle-color': '#f3a6b2', 'circle-opacity': 1.0, 'circle-stroke-width': 1, 'circle-stroke-color': '#232323'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_sougueta",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_sougueta",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_nianaya",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_nianaya",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_dbl",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_dbl",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_mambia",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_mambia",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_linsan",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_linsan",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_koliagb",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_koliagb",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_friguiagb",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_friguiagb",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_centreville",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_centreville",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
,
        {
            "id": "lyr_POSTE_NON_TRAITES_LOCALITES_KINDIA_kolenten",
            "type": "circle",
            "source": "POSTE_NON_TRAITES_LOCALITES_KINDIA_kolenten",
            "layout": {},
            "paint": {'circle-radius': ['/', 23.57142857142857, 2], 'circle-color': '#b80808', 'circle-opacity': 1.0, 'circle-stroke-width': 0.7142857142857143, 'circle-stroke-color': '#b80808'}
        }
],
}